package edu.usu.cs.oo;

public class Job {

	private String jobTitle;
	private int basePay;
	private Company company;
	
	public Job(String jobTitle, int basePay, Company company)
	{
		this.jobTitle = jobTitle;
		this.basePay = basePay;
		this.company = company;
	}
	
	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public int getBasePay() {
		return basePay;
	}

	public void setBasePay(int basePay) {
		this.basePay = basePay;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}
	
	@Override
	public String toString()
	{
		return "Job: " + company + ", " + jobTitle + " for " + basePay + " dollars per year";
	}
}
