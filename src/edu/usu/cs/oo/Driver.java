package edu.usu.cs.oo;

public class Driver {
	
	public static void main(String[] args)
	{
		//Student student = new Student("Chris Thatcher", "AOneMillion", new Job("Dentist", 100000, new Company()));
		
		/*
		 * Instantiate an instance of Student that includes your own personal information
		 * Feel free to make up information if you would like.
		 */
		Student student = new Student("Tyrel Bronson", "AFairyTail",new Job("Gamer", 10, new Company("Boy", new Location("360 Yo Mama", "12345", "Middle", "NoWhere"))));
		
		
		System.out.println(student);
		
		/*
		 * Print out the student information. 
		 */
	}

}